<?php

header('Content-Type: text/html; charset=UTF-8');
session_start();


class ApiController extends User
{

    function __construct()
    {
        $t = $_SERVER['REQUEST_URI'];
        $request = str_replace("/login/api/", "", $t);
        $requestArray = explode("?", $request);
        $function = $requestArray[0];
        if (method_exists($this, $function)) {
            $this->$function();
        } else {
            $this->error();
        }
    }

    private function emailvalid()
    {
        if (isset($_POST['email'])) {
            echo parent::checkEmailValidity($_POST['email']);
        } else {
            $this->error();
        }
    }

    private function passwordvalid()
    {
        if (isset($_POST['password'])) {
            echo parent::checkPasswordValidity($_POST['password']);
        } else {
            $this->error();
        }
    }

    private function namevalid()
    {
        if (isset($_POST['name'])) {
            echo parent::checkNameValidity($_POST['name']);
        } else {
            $this->error();
        }
    }

    private function belepes()
    {
        if (isset($_POST['password']) && isset($_POST['email'])) {
            $pass = json_decode(parent::checkPasswordValidity($_POST['password']));
            $email = json_decode(parent::checkEmailValidity($_POST['email']));

            if ($email->status == 1 && $pass->status == 1) {
                echo parent::checkIn($_POST['email'], $_POST['password']);
            } else {
                echo parent::jsonView(false, "belepes", "Hibás adatok", "main-message");
            }
        } else {
            $this->error();
        }
    }


    private function regisztracio()
    {
        if (isset($_POST['nev']) && isset($_POST['password']) && isset($_POST['email'])) {
            $pass = json_decode(parent::checkPasswordValidity($_POST['password']));
            $email = json_decode(parent::checkEmailValidity($_POST['email']));
            $name = json_decode(parent::checkNameValidity($_POST['nev']));

            if ($email->status == 1 && $pass->status == 1 && $name->status == 1) {
                echo parent::register($_POST['nev'], $_POST['password'], $_POST['email']);
            } else {
                echo parent::jsonView(false, "", "Hibás adatok", "main-message");
            }
        } else {
            $this->error();
        }
    }

    private function error()
    {
        echo parent::jsonView(false, "", "Hiba történt", "main-message");
    }


    private function hackcheck()
    {
        echo parent::hackCount($_POST['email']);
    }


}

function __autoload($className){
    if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/login/inc/' . $className . '.php')) {
        require_once $_SERVER['DOCUMENT_ROOT'] . '/login/inc/' . $className . '.php';
    } else{
        echo "hiba az osztály betöltésekor";
    }
}

$api = new ApiController();
?>