<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Tesztfeladat</title>
    <link rel="stylesheet" href="css/style.css">
    <script src="js/register.js"></script>
</head>
<body>
<div class="content">
    <div class="main-message"></div>
    <div class="name-message"></div>
    <div class="password-message"></div>
    <div class="email-message"></div>
    <div class="captcha-message"></div>

    <div class="belepes active">
        <p class="cim">Belépés</p>

        <div class="input-box">
            <label for="email">Email</label>
            <input type="email" name="email" value=""
                   placeholder="E-mail címe" id="email"
                   autocomplete="off" required
                   onkeyup="reg.emailValid(value)">

            <div class="email-response"></div>
        </div>
        <div class="input-box">
            <label for="password">Jelszó</label>
            <input type="password" name="password" value=""
                   placeholder="Jelszava" id="password"
                   autocomplete="off" required
                   onkeyup="reg.passwordValid(value)"><button onclick="reg.showOrHidePassword()" class="small">Mutat/Rejt</button>
        </div>
        <div class="input-box captcha">
            <label for="captcha">Captcha</label>

            <p class="captcha-content"></p>
            <input type="number" name="captcha" value=""
                   placeholder="" id="captcha"
                   autocomplete="off"
                   onkeyup="reg.captchaValid(value)">
        </div>
        <div class="input-box">
            <button class="belepes-button" onclick="reg.sendLogin()">Belépés</button>
        </div>
    </div>
    <div class="regisztracio">
        <p class="cim">Regisztráció</p>

        <div class="input-box">
            <label for="reg-name">Név</label>
            <input type="text" name="name" value="" placeholder="Ön neve" id="reg-name" required
                   onkeyup="reg.namedValid(value)">
        </div>
        <div class="input-box">
            <label for="reg-email">Email</label>
            <input type="email" name="email" value=""
                   placeholder="E-mail címe" id="reg-email"
                   autocomplete="off" required
                   onkeyup="reg.emailValid(value)">
        </div>
        <div class="input-box">
            <label for="reg-password">Jelszó</label>
            <input type="password" name="password" value=""
                   placeholder="Jelszava" id="reg-password"
                   autocomplete="off" required
                   onkeyup="reg.passwordValid(value)"> <button onclick="reg.showOrHidePassword()" class="small">Mutat/Rejt</button>
        </div>
        <div class="input-box">
            <button class="regisztracio-button" onclick="reg.sendReg()">Regisztráció</button>
        </div>
    </div>
    <div class="nav">
        <button onclick="reg.login()">Belépés</button>
        <button onclick="reg.register()">Regisztráció</button>
    </div>
</div>

</body>
</html>