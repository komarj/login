class Register {
    constructor() {
        this.passwordVariableValid = false;
        this.emailcimValid = false;
        this.nameValid = false;
        this.capchtaValid = true;
        this.emailData = "";
        this.passwordData = "";
        this.nameData = "";
        this.captchaData = "";
        this.hackCheck();
    }

    /*
     *   call belepes panel
     */
    login() {
        document.getElementsByClassName("belepes")[0].classList.add("active");
        document.getElementsByClassName("regisztracio")[0].classList.remove("active");
        document.getElementsByClassName('main-message')[0].innerHTML = "";
        document.getElementsByClassName('password-message')[0].innerHTML = "";
        document.getElementsByClassName('email-message')[0].innerHTML = "";
        document.getElementsByClassName('captcha-message')[0].value = "";
        this.passwordVariableValid = false;
        this.emailcimValid = false;
        this.nameValid = false;
    }

    /*
     *   call regisztracios panel
     */
    register() {
        document.getElementsByClassName("regisztracio")[0].classList.add("active");
        document.getElementsByClassName("belepes")[0].classList.remove("active");
        document.getElementsByClassName('main-message')[0].innerHTML = "";
        document.getElementsByClassName('password-message')[0].innerHTML = "";
        document.getElementsByClassName('email-message')[0].innerHTML = "";
        document.getElementsByClassName('captcha-message')[0].value = "";
        this.passwordVariableValid = false;
        this.emailcimValid = false;
        this.nameValid = false;
    }

    /*
     *  email address validation
     */
    emailValid(value) {
        this.emailData = value;
        let email = {email: value};
        let url = "emailvalid";
        this.sendData(email, url);
    }

    /*
     *  password validation
     */
    passwordValid(value) {
        this.passwordData = value;
        let password = {password: value};
        let url = "passwordvalid";
        this.sendData(password, url);
    }

    /*
     *  bad request counter state
     */
    hackCheck() {
        let data;
        if (this.emailData != "") {
            data = {email: this.emailData};
        }
        let url = "hackcheck";
        this.sendData(data, url);
    }

    /*
     *  name validation
     */
    namedValid(value) {
        this.nameData = value;
        let password = {name: value};
        let url = "namevalid";
        this.sendData(password, url);
    }

    /*
     *  captcha validation
     */

    captchaValid(value) {
        if (this.captchaData == value) {
            this.capchtaValid = true;
            document.getElementsByClassName("captcha-message")[0].innerHTML = "";
        } else {
            this.capchtaValid = false;
            document.getElementsByClassName("captcha-message")[0].innerHTML = "Hibás Captcha";
        }
    }

    /*
     *  send date for login
     */
    sendLogin() {
        if (this.passwordVariableValid == true && this.emailcimValid == true && this.capchtaValid == true) {
            let data = {password: this.passwordData, email: this.emailData};
            let url = "belepes";
            this.sendData(data, url);
            document.getElementById('captcha').value = "";
        } else{
            document.getElementsByClassName('main-message')[0].innerHTML = "Hibás adatok";
        }
    }

    /*
     *  send date for register
     */
    sendReg() {
        if (this.passwordVariableValid == true && this.emailcimValid == true && this.nameValid == true) {
            let data = {nev: this.nameData, password: this.passwordData, email: this.emailData};
            let url = "regisztracio";
            this.sendData(data, url);
        } else{
            document.getElementsByClassName('main-message')[0].innerHTML = "Hibás adatok";
        }
    }

    /*
     *  create and show captcha
     */
    captchaContent() {
        document.getElementsByClassName("captcha-content")[0].innerHTML = "6 + 6 = ?";
        this.captchaData = 12;
    }

    /*
     *  send date for api
     */
    sendData(data, url) {
        let regObj = this;
        let XHR = new XMLHttpRequest();
        let urlEncodedDataPairs = [];
        let urlEncodedData;
        let name;

        for (name in data) {
            urlEncodedDataPairs.push(encodeURIComponent(name) + '=' + encodeURIComponent(data[name]));
        }

        urlEncodedData = urlEncodedDataPairs.join('&').replace(/%20/g, '+');

        /*
         *  if load send to date processMessage function
         */
        XHR.addEventListener('load', function (event) {
            regObj.processMessage(JSON.parse(event.currentTarget.response));
        });


        XHR.addEventListener('error', function (event) {
            console.log(event);
        });


        XHR.open('POST', 'http://localhost/api/' + url);
        XHR.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        XHR.send(urlEncodedData);
    }

    /*
     * show password
     */

    showOrHidePassword(){
        let input = [document.getElementById('reg-password'), document.getElementById('password')];
        for(let i =0; i<input.length; i++){
            if(input[i].type == 'text'){
                input[i].type = 'password';
            }else{
                input[i].type = 'text';
            }
        }


    }

    /*
     *  processing message
     */
    processMessage(message) {

        if (message.locate != undefined) {
            document.getElementsByClassName(message.locate)[0].innerHTML = message.message;
        }

        switch (message.variable) {
            case "regisztracio":
                if (message.status == false) {
                } else {
                    document.getElementById('reg-name').value = "";
                    document.getElementById('reg-email').value = "";
                    document.getElementById('reg-password').value = "";
                }
                break;
            case "password":
                if (message.status == false) {
                    this.passwordVariableValid = false;
                } else {
                    this.passwordVariableValid = true;
                }
                break;
            case "name":
                if (message.status == false) {
                    this.nameValid = false;
                } else {
                    this.nameValid = true;
                }
                break;
            case "belepes":
                if (message.status == true) {
                    window.location.replace('http://localhost/login/iranyitopul');
                } else {
                    this.hackCheck();
                }
                break;
            case "email":
                if (message.status == false) {
                    this.emailcimValid = false;
                } else {
                    this.emailcimValid = true;
                    this.hackCheck();
                }
                break;
            case "hackcheck":
                if (message.status == false) {
                    document.getElementsByClassName("captcha")[0].classList.add("block");
                    this.captchaContent();
                    this.capchtaValid = false;
                }
                break;
            default:
        }
    }
}


/*
 * create
 */
let reg = new Register();
reg.hackCheck();








