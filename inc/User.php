<?php

class User
{
    private $password_small = false;
    private $password_big = false;
    private $password_number = false;
    private $password_special = false;
    private $password_length = false;

    //SQL date
    private $sql_user = "";
    private $sql_password = "";
    private $sql_db = "";


    /**
     * @param $email
     * @return bool
     * Check email validity
     */
    protected function checkEmailValidity($email)
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return $this->jsonView(true, "email", "", "email-message");
        } else {
            return $this->jsonView(false, "email", "Hibás email cím!", "email-message");
        }
    }


    /**
     * @param $email
     * @return bool
     * Check name validity
     */
    protected function checkNameValidity($name)
    {
        if (mb_strlen($name, 'utf8') > 2 && mb_strlen($name, 'utf8') <= 100) {

            return $this->jsonView(true, "name", "", "name-message");
        } else {
            return $this->jsonView(false, "name", "A név minimum 3, max 100 karakter", "name-message");
        }
    }

    /**
     * @param $password
     * @return bool
     * Check Password Validity
     * must include small and large letters, numbers and special characters and lenght min. 12
     */
    protected function  checkPasswordValidity($password)
    {

        preg_replace_callback_array(
            [
                '/[a-zöüóőúéáűí]+/' => function () {
                    $this->password_small = true;
                },
                '/[A-ZÖÜÓŐÚÁŰÉÍ]+/' => function () {
                    $this->password_big = true;
                },
                '/[0-9]+/' => function () {
                    $this->password_number = true;
                },
                '/[^a-zA-Z0-9öüóőúűáéÖÜÓŐÚÉÁŰ]/' => function () {
                    $this->password_special = true;
                }
            ], $password
        );

        if (mb_strlen($password, 'utf8') > 11) {
            $this->password_length = true;
        }

        if ($this->password_small && $this->password_big &&
            $this->password_number && $this->password_special && $this->password_length
        ) {
            return $this->jsonView(true, "password", "", "password-message");
        } else {
            $error = "Hibás jelszó: ";
            if (!$this->password_small) {
                $error .= "Nem tartalmaz kisbetűt ";
            }
            if (!$this->password_big) {
                $error .= "Nem tartalmaz nagybetűt ";
            }
            if (!$this->password_number) {
                $error .= "Nem tartalmaz számot ";
            }
            if (!$this->password_special) {
                $error .= "Nem tartalmaz speciális karaktert (+!%/=...) ";
            }
            if (!$this->password_length) {
                $error .= "Minimum 12 karakter";
            }
            return $this->jsonView(false, "password", $error, "password-message");
        }

    }

    /**
     * @param $password
     * @return string
     * password encryption with md5
     */
    protected function passwordEncryption($password)
    {
        return md5('login' . $password);
    }

    /**
     * @param $email
     * @param $password
     * @return string
     * login method
     */
    protected function checkIn($email, $password)
    {


        $mysqli = new mysqli('localhost', $this->sql_user, $this->sql_password, $this->sql_db);
        if ($mysqli->connect_error) {
            die('Connect Error: ' . $mysqli->connect_error);
        }

        $email=strip_tags($email);
        $email = $mysqli->real_escape_string($email);
        $password = $this->passwordEncryption($password);
        mysqli_set_charset($mysqli, 'utf8');
        $result = mysqli_query($mysqli, "SELECT * FROM user WHERE email like '$email' AND password like '$password'");

        if ($result->num_rows > 0) {
            while ($row = $result->fetch_object()) {
                $adatok[] = $row;
            }
            $_SESSION['adatok'] = $adatok[0];

            if($adatok[0]->activate){
                return $this->jsonView(true, "belepes", "", "main-message");
            }else{
                $this->log($email);
                return $this->jsonView(false, "belepes", "Aktiválja email címét", "main-message");
            }


        } else {
            $this->log($email);
            return $this->jsonView(false, "belepes", "Hibás adatok", "main-message");
        }
        mysqli_close($mysqli);

    }


    /**
     * @param string $email
     * @return string
     * bad request counter
     */
    protected function hackCount($email = "")
    {

        $mysqli = new mysqli('localhost', $this->sql_user, $this->sql_password, $this->sql_db);
        if ($mysqli->connect_error) {
            die('Connect Error: ' . $mysqli->connect_error);
        }
        if ($email = "") {
            mysqli_set_charset($mysqli, 'utf8');

            $email=strip_tags($email);
            $email = $mysqli->real_escape_string($email);

            $result = mysqli_query($mysqli, "SELECT count(email) AS db FROM log WHERE email like '" . $email . "' AND date > NOW() -INTERVAL 30 MINUTE");

            if ($result->num_rows > 0) {
                while ($row = $result->fetch_object()) {

                    $emailCount = $row->db;
                }
            } else {
                $emailCount = 0;
            }
            $result->close();
        }

        $result = mysqli_query($mysqli, "SELECT count(ip) AS db FROM log WHERE ip like '" . $_SERVER['REMOTE_ADDR'] . "' AND date > NOW() -INTERVAL 30 MINUTE");

        if ($result->num_rows > 0) {
            while ($row = $result->fetch_object()) {
                $ipCount = $row->db;
            }
        } else {
            $ipCount = 0;
        }
        $result->close();

        mysqli_close($mysqli);

        if ($ipCount >= 5 or $emailCount >= 3) {
            return $this->jsonView(false, "hackcheck", "", "password-message");
        } else {
            return $this->jsonView(true, "hackcheck", "", "password-message");
        }

    }


    /**
     * @param $status
     * @param $variable
     * @param string $message
     * @param $locate
     * @return string
     * return json
     */
    protected function jsonView($status, $variable, $message, $locate){
        return json_encode(array("status" => $status, "variable" => $variable, "message" => $message, "locate" => $locate));
    }

    /**
     * @param $email
     * bad request loger
     */
    protected function log($email)
    {

        $mysqli = new mysqli('localhost', $this->sql_user, $this->sql_password, $this->sql_db);
        if ($mysqli->connect_error) {
            die('Connect Error: ' . $mysqli->connect_error);
        }
        mysqli_set_charset($mysqli, 'utf8');

        $email = $mysqli->real_escape_string($email);

        $sql_query = "INSERT INTO log(email, ip) VALUES ('" . $email . "','" . $_SERVER['REMOTE_ADDR'] . "')";

        if ($mysqli->query($sql_query) === TRUE) {
        } else {
            echo "Error: " . $sql_query . "<br>" . $mysqli->error;
        }

    }

    /**
     * @param $name
     * @param $passsword
     * @param $email
     * @return string
     * regiszter method
     */
    protected function register($name, $passsword, $email)
    {

        $mysqli = new mysqli('localhost', $this->sql_user, $this->sql_password, $this->sql_db);
        if ($mysqli->connect_error) {
            die('Connect Error: ' . $mysqli->connect_error);
        }
        mysqli_set_charset($mysqli, 'utf8');

        //XSS
        $name=strip_tags($name);
        $email=strip_tags($email);
        //SQL Inject
        $name = $mysqli->real_escape_string($name);
        $email = $mysqli->real_escape_string($email);
        $passsword = $this->passwordEncryption($passsword);
        $session = $this->passwordEncryption(rand(100, 2000));

        $sql_query = "INSERT INTO user(email, password, name, session ) VALUES ('" . $email . "','" . $passsword . "','" . $name . "','" . $session . "')";

        if ($mysqli->query($sql_query) === TRUE) {
            $emalkuldes = $this->sendEmail($name, $email, $session);
            if ($emalkuldes) {
                return $this->jsonView(true, "regisztracio", "Sikeres regisztráció! Hamarosan emailben érkezik az aktiváló link.", "main-message");
            } else {
                return $this->jsonView(false, "regisztracio", "Hiba történt az aktiváló email küldésekor", "main-message");
            }
        } else {
            return $this->jsonView(false, "regisztracio", "Hiba történt,  ezzel az email címmel már regisztráltak", "main-message");
        }

    }

    /**
     * @param $name
     * @param $email
     * @param $session
     * @return bool
     * email sender
     */
    protected function sendEmail($name, $email, $session)
    {
        $body = '<p>Kedves ' . $name . '</p>
                <p>Aktiváló link:</p>
                <a href="http:///aktivalas?session=' . $session . '">Aktiválás</a>
                <p>Ha a fenti hivatkozásra nem tud kattintani, kérjük, hogy az alábbi hivatkozást kézzel másolja át böngészője címsorába.</p>
                <p>http:///aktivalas?session=' . $session . '</p>';
        $subject = "Aktiválás";

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= "From: Kómár János<demo@gmail.com> \r\n";


        if (mail($email, $subject, $body, $headers)) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * @param $session
     * @return bool
     * user activate method
     */
    public function aktival($session)
    {
        $mysqli = new mysqli('localhost', $this->sql_user, $this->sql_password, $this->sql_db);
        if ($mysqli->connect_error) {
            die('Connect Error: ' . $mysqli->connect_error);
        }

        $session = $mysqli->real_escape_string($session);
        mysqli_set_charset($mysqli, 'utf8');
        $result = mysqli_query($mysqli, "SELECT * FROM user WHERE session like '$session'");

        if ($result->num_rows > 0) {
            while ($row = $result->fetch_object()) {
                $adatok[] = $row;
            }
            return $this->activateUser($adatok[0]->email);
        } else {
            return false;
        }
        mysqli_close($mysqli);

    }


    /**
     * @param $email
     * @return bool
     * when activeted user, update databese
     */
    protected function activateUser($email)
    {
        $mysqli = new mysqli('localhost', $this->sql_user, $this->sql_password, $this->sql_db);
        if ($mysqli->connect_error) {
            die('Connect Error: ' . $mysqli->connect_error);
        }
        $sql = "UPDATE user SET activate = 1, session = '' WHERE email = '" . $email . "'";

        if ($mysqli->query($sql) === TRUE) {
            return true;
        } else {
            return false;
        }
    }

}


?>