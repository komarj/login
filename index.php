<?php

session_start();


class FrontendController
{

    public function __construct()
    {
        $t = $_SERVER['REQUEST_URI'];
        $request = str_replace("/login/", "", $t);
        $requestArray = explode("?", $request);
        $function = $requestArray[0];

        if (method_exists($this, $function)) {
            $this->$function();
        } else {
            $this->index();
        }

    }

    private function index()
    {

        if (isset($_SESSION['adatok']) && $_SESSION['adatok']->activate == 1) {
            header("Location: http://localhost/login/iranyitopult");
        } else {
            include_once('template/index.tpl');
        }
    }

    private function iranyitopult()
    {

        if (isset($_SESSION['adatok']) && $_SESSION['adatok']->activate == 1) {
            include_once('template/iranyitopult.tpl');
        } else {
            header("Location: http://localhost/login/");
        }
    }

    private function kilepes()
    {

        unset($_SESSION['adatok']);
        if (!isset($_SESSION['adatok'])) {
            header("Location: http://localhost/login/");
        }

    }

    private function aktivalas()
    {
        $aktivalas = new User();
        $akt = $aktivalas->aktival($_GET['session']);


        if ($akt) {
            $aktivalas = "Sikeres aktiválás";
            $belepes = '<a href="./login">Belépés</a>';
        } else {
            $aktivalas = "Sikertelen aktiválás";
            $belepes = '';
        }
        include_once('template/aktivalas.tpl');

    }
}

function __autoload($className){
    if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/login/inc/' . $className . '.php')) {
        require_once $_SERVER['DOCUMENT_ROOT'] . '/login/inc/' . $className . '.php';
    } else{
        echo "hiba az osztály betöltésekor";
    }
}

$fController = new FrontendController();


?>